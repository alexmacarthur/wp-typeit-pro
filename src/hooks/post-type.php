<?php

namespace TypeIt\Pro;

use TypeIt;

add_action( 'init', 'TypeIt\Pro\register_post_type');
add_action( 'add_meta_boxes', 'TypeIt\Pro\add_meta_box');
add_action( 'save_post', 'TypeIt\Pro\save' );
add_action( 'admin_enqueue_scripts', 'TypeIt\register_typeit_script');
add_action( 'admin_enqueue_scripts', 'TypeIt\Pro\enqueue_preview_script', 12);

function save( $postID ) {
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
	if( !isset( $_POST[Store::get('options_prefix') . '_nonce'] )) return;
	if( !wp_verify_nonce( $_POST[Store::get('options_prefix') . '_nonce'], Store::get('options_prefix') . '_nonce_verification' ) ) return;
	if( !current_user_can( 'edit_posts' ) ) return;

	if(!isset($_POST['typeit_nonce'])) return;

	unset($_POST['typeit_nonce']);

	//-- Remove all non-TypeIt-related items.
	$filteredPost = array_filter($_POST, function ($value, $key) {
		return substr($key, 0, 7) === 'typeit_';
	}, ARRAY_FILTER_USE_BOTH);

	$optionsCopy = \TypeIt\Store::get('option_defaults');

	//-- Rewrite keys to remove 'typeit_' prefix before saving to database.
	foreach($filteredPost as $key => $value) {
		$newKey = substr($key, 7);
		$filteredPost[$newKey] = $value;
		unset($filteredPost[$key]);

		//-- Remove from copy of options so we know which values weren't sent via $_POST.
		unset($optionsCopy[$newKey]);
	}

	//-- Set each non-passed option as 'false'.
	foreach($optionsCopy as $key => $value) {
		$filteredPost[$key] = false;
	}

	//-- Set title to strings so we can sort columns.
	wp_update_post(array(
		'ID'           => $postID,
		'post_title'   => substr($filteredPost['strings'][0], 0, 50)
	));

	return update_post_meta( $postID, Store::get('options_prefix'), $filteredPost );
}

function register_post_type() {
	$labels = array(
		'name'               => _x( 'TypeIt Effects', 'post type general name', 'typeit' ),
		'singular_name'      => _x( 'TypeIt Effect', 'post type singular name', 'typeit' ),
		'menu_name'          => _x( 'TypeIt Effects', 'admin menu', 'typeit' ),
		'name_admin_bar'     => _x( 'TypeIt Effects', 'add new on admin bar', 'typeit' ),
		'add_new'            => _x( 'Add New', 'TypeIt Effect', 'typeit' ),
		'add_new_item'       => __( 'Add New TypeIt Effect', 'typeit' ),
		'new_item'           => __( 'New TypeIt Effect', 'typeit' ),
		'edit_item'          => __( 'Edit TypeIt Effect', 'typeit' ),
		'view_item'          => __( 'View TypeIt Effect', 'typeit' ),
		'all_items'          => __( 'All TypeIt Effects', 'typeit' ),
		'search_items'       => __( 'Search TypeIt Effects', 'typeit' ),
		'parent_item_colon'  => __( 'Parent TypeIt Effects:', 'typeit' ),
		'not_found'          => __( 'No TypeIt Effects Found.', 'typeit' ),
		'not_found_in_trash' => __( 'No TypeIt Effects Found in Trash.', 'typeit' )
	);

	$args = array(
		'labels'             => $labels,
		'description'        => 'TypeIt instances for your WordPress site.',
		'public'             => true,
		'has_archive'        => true,
		'publicly_queryable' => false,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'post',
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array(''),
		'menu_icon' => 'dashicons-welcome-write-blog'
	);

	\register_post_type( Store::get('post_type_slug'), $args );
}

/**
 * Add meta box for the post. Only display on post types that are publicly queryable.
 */
function add_meta_box() {
	
	if(Utilities::get_current_post_type() !== Store::get('post_type_slug')) return;

	if(isset($_GET['post'])) {
		\add_meta_box( 'typeit_shortcode_metabox', 'Typewriter Effect Shortcode', 'TypeIt\Pro\typeit_shortcode_metabox_cb', null, 'normal', 'high' );
		\add_meta_box( 'typeit_preview_metabox', 'Typewriter Effect Preview', 'TypeIt\Pro\typeit_preview_metabox_cb', null, 'normal', 'high' );
	}

	\add_meta_box( 'typeit_metabox', 'Typewriter Effect Settings', 'TypeIt\Pro\typeit_metabox_cb', null, 'normal', 'high' );
}

/**
 * Generate markup for the meta box.
 *
 * @return void
 */
function typeit_metabox_cb() {
	wp_nonce_field( Store::get('options_prefix') . '_nonce_verification', Store::get('options_prefix') . '_nonce' );
	?>

		<div class="BoxContainer" id="typeItMetabox">
			<?php foreach(\TypeIt\Store::get('option_defaults') as $key => $option):
					$canHaveMany = isset($option['can_have_many']) && $option['can_have_many'];
					$name = 'typeit_' . $key . ($canHaveMany ? '[]' : ''); 
					$id = 'typeit_' . $key;
					$value = Options::get_post_option($key);
			?>
				<fieldset class="Box" data-box-id="<?php echo $id; ?>">
					<h5 class="Box-label" for="typeit_<?php echo $key; ?>">
						<?php echo $option["name"]; ?>
						<?php if($option['required']) echo '<span>*</span>'; ?>
					</h5>
					<p><?php echo $option['description']; ?></p>

					<?php
						switch ($option['input_type']) {
							case 'textarea':
								?>
								<div class="Box-fieldWrapper">
									<textarea name="<?php echo $name; ?>"> 
										<?php echo $value; ?>
									</textarea>
								</div>
								<?php
								break;
							case 'number':
								?>
								<div class="Box-fieldWrapper">
									<input <?php if($option['required']) echo 'required'; ?> name="<?php echo $name; ?>" value="<?php echo $value; ?>" type="number" />
								</div>
								<?php
								break;
							case 'text':
							//-- @todo: I'm assuming this'll be an array. That smart?
								$values = $value ?: array('');
								foreach($values as $value): 
								?>
								<div class="Box-fieldWrapper">
									<input <?php if($option['required']) echo 'required'; ?> name="<?php echo $name; ?>" value="<?php echo $value; ?>" type="text" />
									<span class="Appender Appender--minus" data-plus-id="<?php echo $id; ?>[]"></span>
								</div>
								<?php
								endforeach;
								break;
							case "checkbox":
								$isChecked = in_array($value, array('on', true));
								?>
								<div class="Box-fieldWrapper">
									<input <?php if($option['required']) echo 'required'; ?> id="typeit_<?php echo $key; ?>" name="<?php echo $name; ?>" <?php checked($isChecked); ?> type="checkbox" />
									<?php if($option['helper_description']) {
										echo '<label class="Box-inputHelper" for="typeit_' . $key . '">' . $option['helper_description'] . '</label>';
									}
								?>
								</div>

							<?php break;
						}
					?>
					<?php if($canHaveMany): ?>
						<div class="AppenderWrapper">
							<span class="Appender" data-plus-id="<?php echo $id; ?>"></span>
							<p class="description">Add Another String</p>
						</div>
					<?php endif; ?>
				</fieldset>
			<?php endforeach; ?>
		</div>
	<?php
}

/**
 * Generate markup for the meta box.
 *
 * @return void
 */
function typeit_preview_metabox_cb() {
	?>
		<div id="typeItMetabox">
			<div>
				<span id="tiPreview"></span>
			</div>

			<button class="button" id="tiReset">Reset</button>
		</div>
	<?php
}

/**
 * Generate markup for the meta box.
 *
 * @return void
 */
function typeit_shortcode_metabox_cb() {
	global $post;
	?>
		<div id="typeItShortcodeMetabox">
			<p>Use this shortcode to render this effect on any page.</p>

			<p><code>[typeit id="<?php echo $post->ID; ?>"]</code></p>
		</div>
	<?php
}

function enqueue_preview_script(){
	$options = Options::get_post_options();

	if(empty($options)) return;

	$typedOptions = \TypeIt\Utilities::get_typed_options($options);

	wp_enqueue_script('typeit');
	
	wp_add_inline_script(
		'typeit',
		'
		function initTypeItPreview () {
			window.tiPrev = new TypeIt("#tiPreview", ' . json_encode($typedOptions) . ');
		}

		initTypeItPreview();

		var $reset = document.getElementById("tiReset");

		if($reset !== null) {
			$reset.addEventListener("click", function (event) {
				event.preventDefault();

				tiPrev.destroy();
				initTypeItPreview();
			});
		}
		'
	);
}
