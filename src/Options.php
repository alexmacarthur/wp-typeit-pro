<?php 

namespace TypeIt\Pro;

class Options {

    /**
     * Gets serialized options for individual post/page.
     *
     * @return array
     */
    public static function get_post_options($postID = null)
    {
        if (is_null($postID)) {

            global $post;

            //-- No post is set. Get outta here.
            if(empty($post)) return array();

            $id = $post->ID;
        } else {
            $post = get_post($postID);
            $id = $post->ID;
        }

        $meta = get_post_meta($id, Store::get('options_prefix'), true);

        //-- If nothing's here, make sure it's an empty array that we return.
        if (!$meta) {
            $meta = array();
        } else {
            $meta['id'] = $id;
        }

        return $meta;
    }

    /**
     * Gets value for specific post/page option.
     *
     * @param  string $key Field key
     * @return string|bool
     */
    public static function get_post_option($key, $postID = null)
    {
        $post_options = self::get_post_options($postID);

        if (!empty($post_options)) {
            return $post_options[$key];
        }

        $defaults = \TypeIt\Store::get('option_defaults');
        if (array_key_exists($key, $defaults)) {
            return $defaults[$key]["default_value"];
        }

        return false;
    }

    /**
     * Gets serialized settings in options table.
     *
     * @return array
     */
    public static function get_options()
    {
        return get_option(Store::get('options_prefix')) ?: array();
    }

    /**
     * Gets specific option value.
     *
     * @param  string $key Option key
     * @return string
     */
    public static function get_option($key)
    {
        return isset(self::get_options()[$key]) ? self::get_options()[$key] : null;
    }
}