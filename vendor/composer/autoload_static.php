<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit07519cf79f74fe609ff72b4aff34893b
{
    public static $files = array (
        '89ff252b349d4d088742a09c25f5dd74' => __DIR__ . '/../..' . '/packages/plugin-update-checker/plugin-update-checker.php',
    );

    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'TypeIt\\Pro\\' => 11,
        ),
        'C' => 
        array (
            'Composer\\Installers\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'TypeIt\\Pro\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
        'Composer\\Installers\\' => 
        array (
            0 => __DIR__ . '/..' . '/composer/installers/src/Composer/Installers',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit07519cf79f74fe609ff72b4aff34893b::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit07519cf79f74fe609ff72b4aff34893b::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
