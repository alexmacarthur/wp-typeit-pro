<?php

namespace TypeIt\Pro;

class Utilities
{
    public static function get_current_post_type()
    {
        global $post, $typenow, $current_screen;

        if ($post && $post->post_type) {
            return $post->post_type;
        } elseif ($typenow) {
            return $typenow;
        } elseif ($current_screen && $current_screen->post_type) {
            return $current_screen->post_type;
        } elseif (isset($_REQUEST['post_type'])) {
            return sanitize_key($_REQUEST['post_type']);
        } elseif (isset($_REQUEST['post'])) {
            return get_post_type($_REQUEST['post']);
        }
        return null;
    }

    /**
     * Gets full name of particular field, with prefix appended.
     *
     * @param  string $name Name of field
     * @return string
     */
    public static function get_field_name($name)
    {
        return Store::get('options_prefix') . '[' . $name . ']';
    }

}
