<?php

namespace TypeIt\Pro;

add_filter( 'manage_' . Store::get('post_type_slug') . '_posts_columns' , 'TypeIt\Pro\manage_columns');
add_action( 'manage_posts_custom_column' , 'TypeIt\Pro\customize_columns', 10, 2 );
add_filter( 'manage_edit-' . Store::get('post_type_slug') . '_sortable_columns', 'TypeIt\Pro\make_columns_sortable' );
add_action( 'pre_get_posts', 'TypeIt\Pro\modify_query_to_make_sortable', 1);

function customize_columns($column, $id) {

	if(get_post_type($id) !== Store::get('post_type_slug')) return;

	switch ( $column ) {
		case 'strings':
			$arrayToString = implode(' ', Options::get_post_option('strings', $id) ?: []);
			echo '<strong><a href="' . get_edit_post_link($id) . '">';
			echo mb_strimwidth($arrayToString, 0, 50, '...');
			echo '</a></strong>';
			break;
		case 'shortcode':
			echo '[typeit id="' . $id . '"]';
			break;
	}
}

function manage_columns($columns) {
	unset($columns['title']);
	$columns['strings'] = 'Strings';
	$columns['shortcode'] = 'Shortcode';

	//-- Move date column to the end.
	$date = $columns['date'];
	unset($columns['date']);
	$columns['date'] = $date;

	return $columns;
}

function make_columns_sortable($columns) {
	$columns['strings'] = 'strings';
	$columns['shortcode'] = 'shortcode';

	return $columns;
}

function modify_query_to_make_sortable($query) {
	if(!is_admin()) return;
	if(!$query->is_main_query()) return;
	if($query->query['post_type'] !== Store::get('post_type_slug')) return;

	switch( $query->get( 'orderby' ) ) {
		case 'shortcode':
			$query->set( 'orderby', 'ID' );
			break;
		case 'strings':
			$query->set( 'orderby', 'title' );
			break;
	}
}