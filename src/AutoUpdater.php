<?php 

namespace TypeIt\Pro;

class AutoUpdater {

    private $updateChecker;

    public function __construct() {

        $this->updateChecker = \Puc_v4_Factory::buildUpdateChecker(
            'https://bitbucket.org/alexmacarthur/wp-typeit-pro',
            plugin_dir_path(dirname(__FILE__)) . 'typeit-pro.php',
            'wp-typeit-pro'
        );

        $this->updateChecker->setBranch('master');
    }
}