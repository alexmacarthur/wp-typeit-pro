<?php 

namespace TypeIt\Pro;

add_action( 'admin_init', '\TypeIt\Pro\ensure_free_version_is_disabled');
add_action( 'admin_enqueue_scripts', '\TypeIt\Pro\set_up_admin_styles_and_scripts' );
add_action( 'admin_notices', '\TypeIt\Pro\maybe_show_admin_notice');

add_filter( 'plugin_row_meta', 'TypeIt\Pro\modify_plugin_meta', 10, 4);

function modify_plugin_meta($plugin_meta, $plugin_file, $plugin_data, $status) {
    if(strpos($plugin_file, 'wp-typeit-pro') === false) return $plugin_meta;
    
    return array_filter($plugin_meta, function($item){
        return !strpos($item, "View details");
    });
}

function ensure_free_version_is_disabled() {
    $basePlugin = 'wp-typeit/typeit.php';

    if(is_plugin_active( $basePlugin )) {
        set_transient( 'typeit_deactivation_notice', true, 5 );
        deactivate_plugins($basePlugin);
    }
}

/**
 * If the plugin was activated when the free version was active,
 * show notice about it being automatically deactivated.
 */
function maybe_show_admin_notice() {
    if(!get_transient('typeit_deactivation_notice')) return;
    ?>
        <div class="notice notice-warning is-dismissible">
            <p><?php _e( 'WP TypeIt Pro requires that the free version of WP TypeIt is inactive. It has been automatically deactivated.', 'wp-typeit' ); ?></p>
        </div>
    <?php
    delete_transient( 'typeit_deactivation_notice' );
}

/**
 * On activate, ensure the non-pro version of this plugin is not active.
 *
 * @return void
 */
function on_activation() {
    $basePlugin = 'wp-typeit/typeit.php';

    if(is_plugin_active( $basePlugin )) {
        set_transient( 'typeit_deactivation_notice', true, 5 );
        deactivate_plugins($basePlugin);
    }
}

/**
 * Enqueue necessary admin scripts & styles.
 *
 * @return void
 */
function set_up_admin_styles_and_scripts() {
    $path = plugin_dir_url( __DIR__ );
    wp_enqueue_style( 'typeit', $path . 'src/assets/scss/style.min.css', array(), Plugin::$plugin_data['Version']);
    wp_enqueue_script(
        'typeit-admin',
        $path . 'src/assets/js/scripts.min.js',
        array(),
     Plugin::$plugin_data['Version'],
        true 
    );
}