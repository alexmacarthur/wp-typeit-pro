<?php
/**
* Plugin Name: WP TypeIt Pro
* Description: Easily create and manage typewriter effects using the JavaScript utility, TypeIt.
* Version: 1.0.3
* Author: Alex MacArthur
* Author URI: https://macarthur.me
* License: GPLv2 or later
* License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

namespace TypeIt\Pro;

if ( !defined( 'WPINC' ) ) {
  die;
}

require_once(ABSPATH . 'wp-admin/includes/plugin.php');
require_once(dirname(__FILE__) . '/vendor/autoload.php');

require_once dirname(__FILE__) . '/packages/wp-typeit/typeit.php';

class Plugin {

  public static $plugin_data = null;
  
  public static function go() {
    $GLOBALS[__CLASS__] = new self;
    return $GLOBALS[__CLASS__];
  }

  public function __construct() {
    self::$plugin_data = get_plugin_data(__DIR__ . '/typeit-pro.php');

    new \TypeIt\Pro\AutoUpdater();

    $directory = plugin_dir_path(__FILE__);
    require_once($directory . 'src/hooks/misc.php');
    require_once($directory . 'src/hooks/post-type.php');
    require_once($directory . 'src/hooks/admin-columns.php');
    require_once($directory . 'src/hooks/admin-customizations.php');
  }

}

Plugin::go();

register_activation_hook( __FILE__, 'TypeIt\Pro\on_activation' );