<?php

namespace TypeIt\Pro;

class Store
{
    
    private static $data = array(
        'options_prefix' => 'typeit',
        'post_type_slug' => 'typeit-effect'
    );

    public static function get($key)
    {
        return !empty(self::$data[$key]) ? self::$data[$key] : false;
    }
}
