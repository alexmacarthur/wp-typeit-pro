class TypeItPro {
    constructor() {
        this.attachEventListeners();
    }

    /**
     * Add event listener to each .Appender found on the page. 
     */
    attachEventListeners() {
        const $pluses = document.querySelectorAll('.Appender');
        [].forEach.call($pluses, $plus => {
            $plus.addEventListener('click', e => this.stringRowHandler(e));
        });
    }

    /**
     * If clicked element has '--minus' variation, remove the row. If not, append a new row. 
     * 
     * @param {object} click
     */
    stringRowHandler(e) {

        if (e.target.classList.contains('Appender--minus')) {
            let parentBox = e.target.parentNode;
            parentBox.parentNode.removeChild(parentBox);
            return;
        }

        let idOfField = e.target.getAttribute('data-plus-id');
        let wrappers = document.querySelectorAll(`[data-box-id="${idOfField}"] .Box-fieldWrapper`);
        let wrapper = [].slice.call(wrappers).pop();
        let clone = wrapper.cloneNode(true);
        clone.querySelector('input').value = '';

        wrapper.parentNode.insertBefore(clone, wrapper.nextSibling);

        clone.querySelector('.Appender').addEventListener('click', e => this.stringRowHandler(e));
    }
}

new TypeItPro();